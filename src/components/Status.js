import React, { Component } from "react";

class Status extends Component {
  constructor() {
    super();
    this.state = {
      likes: 0
    };

    this.like = this.like.bind(this); //binds like function to each instance of the class
  }

  like() {
    this.setState({
      likes: this.state.likes + 1
    });
  }

  render() {
    return (
      <div className="status">
        <p>{this.props.myText}</p>
        <p>
          <button onClick={this.like}>{this.state.likes} Likes</button>
        </p>
      </div>
    );
  }
}

export default Status;
