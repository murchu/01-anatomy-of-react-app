import React, { Component } from 'react';
import StatusList from './StatusList';

export default class StatusAll extends Component {
  //alternate way to export component from file
  constructor(props) {
    super(props);
    this.state = {
      newStatusText: '',
      statuses: [ 'Just back from buying food!', 'What a great day', 'Status sucks', 'Time to log off' ]
    };

    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(ev) {
    //debugger; //adds debugger to break here
    this.setState({ newStatusText: ev.target.value });
  }

  handleSubmit = (ev) => {
    //prevent form from taking us to a new page (or refreshing)
    ev.preventDefault();
    const newStatuses = [ this.state.newStatusText, ...this.state.statuses ];
    this.setState({ newStatusText: '', statuses: newStatuses });
  };

  render() {
    return (
      <React.Fragment>
        <div className="status">
          <h1>Enter new status</h1>
          <form onSubmit={this.handleSubmit}>
            <input
              type="text"
              value={this.state.newStatusText}
              onChange={this.handleChange}
              placeholder="what's on your mind?"
            />
            <button type="submit">Add Status</button>
          </form>
        </div>
        <div className="status">
          <StatusList statuses={this.state.statuses} />
        </div>
      </React.Fragment>
    );
  }
}
