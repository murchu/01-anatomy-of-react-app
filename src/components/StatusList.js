import React, { Component } from 'react';
import Status from './Status';

class StatusList extends Component {
  render() {
    return this.props.statuses.map((status, index) => {
      return <Status key={index} myText={status} />;
    });
  }
}

export default StatusList;
